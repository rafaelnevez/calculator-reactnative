import React, {Component} from 'react';
import {TouchableHighlight, Dimensions, StyleSheet, Text} from 'react-native';

const styles = StyleSheet.create({
    button: {
        fontSize:40,
        height: Dimensions.get('window').height/8,
        width: Dimensions.get('window').width/5,
        padding:10,
        backgroundColor: '#f0f0f0',
        textAlign: 'center',
        borderWidth:1,
        borderColor: '#888',
    },
    operationButton:{
        color:"#fff",
        backgroundColor:"#fa8231",
    },
    buttonClear:{
        color:"#fff",
        backgroundColor:"#ea3c3c",
    },
    buttonDouble:{
        width: (Dimensions.get('window').width/5)*2,
    },
    buttonTriple:{
        width: (Dimensions.get('window').width/5)*3,
    },
    buttonEqual:{
        color:"#fff",
        backgroundColor:"#12b34b",
    },
    buttonTrig:{
        fontSize:30,

        color:"#fff",
        backgroundColor:"#68cae6",
    },
    buttonMode:{
        fontSize:25,
    },
    
   
})


export default props => {
    const styleButton = [styles.button]

    if(props.double) styleButton.push(styles.buttonDouble)
    if(props.triple) styleButton.push(styles.buttonTriple)
    if(props.operation) styleButton.push(styles.operationButton)
    if(props.clear) styleButton.push(styles.buttonClear)
    if(props.equal) styleButton.push(styles.buttonEqual)
    if(props.trig) styleButton.push(styles.buttonTrig)
    if(props.mode) styleButton.push(styles.buttonMode)


    return (
        <TouchableHighlight onPress={() => props.onClick(props.label)}>
            <Text style={styleButton}>{props.label}</Text>
        </TouchableHighlight>
    )
}
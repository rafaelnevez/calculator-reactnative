import React, { Component } from 'react';
import { View, StyleSheet, Text, Animated } from 'react-native';

const style = StyleSheet.create({
    display: {
        fontSize: 40,
        padding: 20,
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.6)',
        alignItems: 'flex-end',
    },
    displayValue: {
        fontSize: 60,
        color: "#fff",
    }
})


export default props => {
    return (
        <View style={style.display}>
            <Text style={style.displayValue} numberOfLines={1}>
                {props.value}
            </Text>
        </View>
    )
}
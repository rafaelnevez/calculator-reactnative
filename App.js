
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Alert } from 'react-native';
import Button from './src/Components/Button'
import Display from './src/Components/Display'


const initalState = {
  displayValue: "0",
  clearDisplay: false,
  operation: null,
  values: [0, 0],
  current: 0,
  mode: "RAD",
}

export default class App extends Component {

  state = { ...initalState }

  addDigit = n => {
    //console.debug("") react-native log-android


    if (this.state.displayValue.split('').length >= 10 && this.state.current === 0) {
      return
    }

    const clearDisplay = this.state.displayValue === "0"
      || this.state.clearDisplay


    if (n === '.' && !clearDisplay && this.state.displayValue.includes('.')) {
      return
    }

    if (n === "." && this.state.displayValue === "0") {
      n = "0."
    }
    const currentValue = clearDisplay ? '' : this.state.displayValue
    const displayValue = currentValue + n
    this.setState({ displayValue: displayValue, clearDisplay: false })
    if (n !== '.') {
      const newValue = parseFloat(displayValue)
      const values = [...this.state.values]
      values[this.state.current] = newValue
      this.setState({ values })
    }



  }

  clearMemory = () => {
    this.setState({ ...initalState })
  }

  cleardigit = () => {

    displayValue = this.state.displayValue

    if (displayValue === '0') {
      return
    }

    displayArray = displayValue.split('')
    if (displayArray.length <= 1) {
      this.setState({ displayValue: "0" })
      return
    }

    displayArray.pop()
    valorFinal = displayArray.join('')

    const newValue = parseFloat(valorFinal)
    const values = [...this.state.values]
    values[this.state.current] = newValue

    this.setState({ displayValue: valorFinal, values })

  }

  setOperation = operation => {
    if (this.state.current === 0) {
      this.setState({ operation, current: 1, clearDisplay: true })
    } else {
      const equals = operation === "="
      const values = [...this.state.values]
      try {
        if (this.state.operation === "%") {
          values[0] = eval(`${values[0]} * ${values[1]} / 100`)
        } else {
          values[0] = eval(`${values[0]} ${this.state.operation} ${values[1]}`)
        }
      } catch (e) {
        values[0] = this.state.values[0]
      }
      values[1] = 0
      this.setState({
        displayValue: `${values[0]}`.split('').length > 10 ? `${values[0].toFixed(8)}` : `${values[0]}`,
        operation: equals ? null : operation,
        current: equals ? 0 : 1,
        //clearDisplay:!equals,
        clearDisplay: true,
        values,
      })
    }
  }



  othersOperations = (operation) => {
    displayValue = this.state.displayValue
    displayArray = displayValue.split('')
    if (displayArray.length === 1 && displayArray[0] === ".") {
      Alert.alert("Erro!", "Por favor insira um numéro válido")
      return
    }

    switch (operation) {
      case "sqr":
        newValue = Math.sqrt(displayValue)
        break;
      case "x²":
        newValue = Math.pow(displayValue, 2)
        break;
      case "1/x":
        newValue = 1 / displayValue
        break;
    }

    this.setState({
      displayValue: `${newValue}`.split('').length > 10 ? `${newValue.toFixed(8)}` : `${newValue}`,
      operation: null,
      current: 0,
      clearDisplay: true,
      values: [newValue, 0],
    })

  }

  degrees = (degrees) => {
    return degrees * Math.PI / 180;
  };


  trignometria = entidade => {
    displayValue = this.state.displayValue
    displayArray = displayValue.split('')
    if (displayArray.length === 1 && displayArray[0] === ".") {
      Alert.alert("Erro!", "Por favor insira um numéro válido")
      return
    }

    switch (entidade) {
      case "S(x)":
        newValue = Math.sin(displayValue)

        break;
      case "C(x)":
        newValue = Math.cos(parseFloat(displayValue))
        break;
      case "T(x)":
        newValue = Math.sin(parseFloat(displayValue)) / Math.cos(parseFloat(displayValue))
        break;
    }

    if (this.state.mode === "DEG")
      displayValue = this.degrees(displayValue)

    this.setState({
      displayValue: `${newValue}`.split('').length > 10 ? `${newValue.toFixed(8)}` : `${newValue}`,
      operation: null,
      current: 0,
      clearDisplay: true,
      values: [newValue, 0],
    })
  }

  trocaSinal = () => {
    displayValue = this.state.displayValue
    displayArray = displayValue.split('')



    newValue = parseFloat(this.state.displayValue) * - 1
    values = [...this.state.values]
    values[this.state.current] = newValue
    this.setState({
      displayValue: `${newValue}`.split('').length > 10 ? `${newValue.toFixed(8)}` : `${newValue}`,
      values,
    })
  }

  mode = () => {
    mode = this.state.mode

    if (mode === "RAD")
      this.setState({ mode: "DEG" })
    else
      this.setState({ mode: "RAD" })

  }

  render() {

    mode = "MODE:\n" + this.state.mode

    return (
      <View style={styles.container}>
        <Display value={this.state.displayValue}>

        </Display>
        <View style={styles.button}>
          <Button label='AC' double onClick={this.clearMemory}></Button>
          <Button label='<=' clear onClick={this.cleardigit}></Button>
          <Button label={mode} mode double onClick={this.mode}></Button>

          <Button label='sqr' operation onClick={this.othersOperations}></Button>
          <Button label='x²' operation onClick={this.othersOperations}></Button>
          <Button label='1/x' operation onClick={this.othersOperations}></Button>
          <Button label='%' operation onClick={this.setOperation}></Button>

          <Button label='/' operation onClick={this.othersOperations}></Button>

          <Button label='7' onClick={this.addDigit}></Button>
          <Button label='8' onClick={this.addDigit}></Button>
          <Button label='9' onClick={this.addDigit}></Button>
          <Button label='S(x)' trig operation onClick={this.trignometria}></Button>
          <Button label='*' operation onClick={this.setOperation}></Button>

          <Button label='4' onClick={this.addDigit}></Button>
          <Button label='5' onClick={this.addDigit}></Button>
          <Button label='6' onClick={this.addDigit}></Button>
          <Button label='C(x)' trig operation onClick={this.trignometria}></Button>
          <Button label='-' operation onClick={this.setOperation}></Button>

          <Button label='1' onClick={this.addDigit}></Button>
          <Button label='2' onClick={this.addDigit}></Button>
          <Button label='3' onClick={this.addDigit}></Button>
          <Button label='T(x)' trig operation onClick={this.trignometria}></Button>
          <Button label='+' operation onClick={this.setOperation}></Button>

          <Button label='+/-' onClick={this.trocaSinal}></Button>
          <Button label='0' onClick={this.addDigit} ></Button>
          <Button label='.' onClick={this.addDigit}></Button>
          <Button label='=' equal double operation onClick={this.setOperation}></Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  },
  button: {
    flexDirection: "row",
    flexWrap: "wrap",
  }
});
